use irc::client::prelude::*;
use std::env::{self, VarError};

// env variable keys
const IRC_NICKNAME: &str = "IRC_NICKNAME";
const IRC_USERNAME: &str = "IRC_USERNAME";
const IRC_SERVER: &str = "IRC_SERVER";
const IRC_PORT: &str = "IRC_PORT";
const IRC_PASSWORD: &str = "IRC_PASSWORD";
const IRC_USE_TLS: &str = "IRC_USE_TLS";
const IRC_CHANNELS: &str = "IRC_CHANNELS";
const IRC_USER_INFO: &str = "IRC_USER_INFO";
const DRIVE_FOLDER_URL: &str = "DRIVE_FOLDER_URL";
const DRIVE_AUTH_HEADER: &str = "DRIVE_AUTH_HEADER";
const DRIVE_FILE_PREFIX: &str = "DRIVE_FILE_PREFIX";

pub struct CliVariables {
    pub irc_config: Config,
    pub drive_folder_url: String,
    pub drive_auth_header: String,
    pub drive_file_prefix: String,
}

pub fn parse_cli_arguments() -> CliVariables {
    let irc_nickname = match env::var(IRC_NICKNAME) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_NICKNAME"),
            _ => panic!("Unable to interpret IRC_NICKNAME env var error: {}", e),
        },
    };
    let irc_username = match env::var(IRC_USERNAME) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_USERNAME"),
            _ => panic!("Unable to interpret IRC_USERNAME env var error: {}", e),
        },
    };
    let irc_server = match env::var(IRC_SERVER) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_SERVER"),
            _ => panic!("Unable to interpret IRC_SERVER env var error: {}", e),
        },
    };
    let irc_port = match env::var(IRC_PORT) {
        Ok(val) => match val.parse::<u16>() {
            Ok(val) => val,
            Err(e) => panic!("Unable to convert IRC_PORT into u16 error: {}", e),
        },
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_PORT"),
            _ => panic!("Unable to interpret IRC_PORT env var error: {}", e),
        },
    };
    let irc_password = match env::var(IRC_PASSWORD) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_PASSWORD"),
            _ => panic!("Unable to interpret IRC_PASSWORD env var error: {}", e),
        },
    };
    // let irc_use_tls = match env::var(IRC_USE_TLS) {
    //     Ok(val) => val,
    //     Err(e) => match e {
    //         VarError::NotPresent => panic!("Unable to find env variable IRC_USE_TLS"),
    //         _ => panic!("Unable to interpret IRC_USE_TLS env var error: {}", e),
    //     },
    // };
    let irc_channels = match env::var(IRC_CHANNELS) {
        Ok(val) => val.split(',').map(|s| s.to_owned()).collect(),
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_CHANNELS"),
            _ => panic!("Unable to interpret IRC_CHANNELS env var error: {}", e),
        },
    };
    let irc_user_info = match env::var(IRC_USER_INFO) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable IRC_USER_INFO"),
            _ => panic!("Unable to interpret IRC_USER_INFO env var error: {}", e),
        },
    };
    let drive_folder_url = match env::var(DRIVE_FOLDER_URL) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable DRIVE_FOLDER_URL"),
            _ => panic!("Unable to interpret DRIVE_FOLDER_URL env var error: {}", e),
        },
    };
    let drive_auth_header = match env::var(DRIVE_AUTH_HEADER) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable DRIVE_AUTH_HEADER"),
            _ => panic!("Unable to interpret DRIVE_AUTH_HEADER env var error: {}", e),
        },
    };
    let drive_file_prefix = match env::var(DRIVE_FILE_PREFIX) {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => panic!("Unable to find env variable DRIVE_FILE_PREFIX"),
            _ => panic!("Unable to interpret DRIVE_FILE_PREFIX env var error: {}", e),
        },
    };

    let irc_config = Config {
        nickname: Some(irc_nickname),
        username: Some(irc_username),
        server: Some(irc_server),
        port: Some(irc_port),
        password: Some(irc_password),
        use_tls: Some(true),
        channels: irc_channels,
        user_info: Some(irc_user_info),
        ..Config::default()
    };

    CliVariables {
        irc_config,
        drive_folder_url,
        drive_auth_header,
        drive_file_prefix,
    }
}
