use crate::RENDERING_CONST;
use anyhow::{Context, Result};
use bmp::Image;
use rand::seq::IteratorRandom;
use std::fs;

pub fn flip_image(img: &Image) -> Image {
    // horrible hack to flip image
    let mut img_flipped = Image::new(RENDERING_CONST.screen_width, RENDERING_CONST.screen_height);
    for (x, y) in img.coordinates() {
        // fill with white
        // println!("height {}, y: {}",img2.get_height(),y);
        img_flipped.set_pixel(
            x,
            y,
            img.get_pixel(img.get_width() - 1 - x, img.get_height() - 1 - y),
        );
    }
    img_flipped
}

pub fn add_icon(img: &mut Image) -> Result<()> {
    let icons = fs::read_dir("resources").context("Failed to read resources directory")?;
    let mut rng = rand::thread_rng();
    let icon_path = icons
        .choose(&mut rng)
        .context("failed to get random icon")?
        .context("failed to unwrap path to random icon")?;
    let icon = bmp::open(icon_path.path()).context("failed to open icon from path")?;
    for (x, y) in icon.coordinates() {
        img.set_pixel(
            RENDERING_CONST.screen_width - 80 + x as u32,
            y + 10,
            icon.get_pixel(x, y),
        );
    }
    Ok(())
}
