use crate::bmp_helper::add_icon;
use crate::COLOURS;
use crate::RENDERING_CONST;
use anyhow::Result;
use bmp::{px, Image, Pixel};
use rusttype::PositionedGlyph;
use rusttype::{point, Font, Scale};

pub struct RasterisedLine<'a> {
    glyphs: Vec<PositionedGlyph<'a>>,
    width_px: u32,
    height_px: u32,
    colours: &'a [usize],
}

pub struct TextFormatting<'a> {
    font: Font<'a>,
    scale: Scale,
}

/// This can panic if the font fails to load, I'm personally ok with it panicking as the program is
/// 1 useless without it and 2 it will panic as soon as it launches
pub fn get_text_formatting() -> TextFormatting<'static> {
    let font =
        Font::try_from_bytes(include_bytes!("../Sauce-Code-Pro-Nerd-Font-Complete.ttf") as &[u8])
            .expect("Error constructing Font");

    TextFormatting {
        font,
        scale: Scale::uniform(45.0),
    }
}

pub fn rasterise_string(
    bmp_path: &str,
    message_string: &str,
    colour_array: &mut [usize],
    text_formatting: &TextFormatting,
) -> Result<Image> {
    // The text to render
    let text = message_string;
    println!("{}", text);
    println!("{}", text.chars().count());
    let wrapped_text = textwrap::wrap(text, RENDERING_CONST.chars_per_line);

    let v_metrics = text_formatting.font.v_metrics(text_formatting.scale);
    let glyphs_height = (v_metrics.ascent - v_metrics.descent).ceil() as u32;

    // let mut lines: Vec<Vec<PositionedGlyph<>>>= Vec::with_capacity(wrapped_text.len());
    let mut lines2: Vec<RasterisedLine> = Vec::with_capacity(wrapped_text.len());

    let mut offset = 0;
    for i in 0..wrapped_text.len() {
        let glyphs: Vec<_> = text_formatting
            .font
            .layout(
                &wrapped_text[i],
                text_formatting.scale,
                point(0.0, 0.0 + v_metrics.ascent),
            )
            .collect();
        // lines.push(glyphs);
        let glyphs_width = {
            let min_x = glyphs
                .first()
                .map(|g| g.pixel_bounding_box().unwrap().min.x)
                .unwrap();
            let max_x = glyphs
                .last()
                .map(|g| g.pixel_bounding_box().unwrap().max.x)
                .unwrap();
            (max_x - min_x) as u32
        };

        lines2.push(RasterisedLine {
            glyphs,
            width_px: glyphs_width,
            height_px: glyphs_height,
            // fix colours with newlines
            colours: &colour_array[offset..wrapped_text[i].chars().count() + offset],
        });
        offset = offset + wrapped_text[i].chars().count() + 1;
    }

    glyphs_to_bmp(bmp_path, lines2)
}

pub fn glyphs_to_bmp(bmp_path: &str, lines: Vec<RasterisedLine>) -> Result<Image> {
    let mut img = Image::new(RENDERING_CONST.screen_width, RENDERING_CONST.screen_height);
    for (x, y) in img.coordinates() {
        // fill with white
        img.set_pixel(x, y, px!(255, 255, 255));
    }
    let image_padding: u32 = 20;
    // Loop through the glyphs in the text, positing each one on a line
    // let mut x_minus: u32 = 0;
    // for glyph in glyphs {
    let mut y_offset = image_padding; // initial padding of 20
    for y_line in 0..lines.len() {
        for x_line in 0..lines[y_line].glyphs.len() {
            let glyph = &lines[y_line].glyphs[x_line];
            if let Some(bounding_box) = glyph.pixel_bounding_box() {
                // Draw the glyph into the image per-pixel by using the draw closure
                glyph.draw(|x, y, v| {
                    let val: [u8; 3] = if v > 0.4 {
                        COLOURS[lines[y_line].colours[x_line]]
                    } else {
                        [255, 255, 255]
                    };
                    if x + image_padding + (bounding_box.min.x as u32)
                        < RENDERING_CONST.screen_width
                    {
                        if y + (bounding_box.min.y as u32) + y_offset
                            < RENDERING_CONST.screen_height
                        {
                            img.set_pixel(
                                x + image_padding + bounding_box.min.x as u32,
                                y + bounding_box.min.y as u32 + y_offset,
                                px!(val[0], val[1], val[2]),
                            );
                        } else {
                            println! {"y overflow!!"};
                        }
                    } else {
                        println! {"x overflow!!"};
                    }
                });
            }
        }
        y_offset += lines[y_line].height_px as u32;
    }

    if let Err(err) = add_icon(&mut img) {
        println!("Failed to add icon: {err:?}")
    };

    match bmp::open(bmp_path) {
        Ok(old) => {
            // the bellow assumes both images are same width
            for (x, y) in old.coordinates() {
                if y + y_offset < img.get_height() {
                    // type is U8 so cant test for < 0 (negative)
                    img.set_pixel(x, y_offset + y, old.get_pixel(x, y));
                }
            }
        }
        Err(err) => println!("failed to open old file, {err:?}"),
    }

    Ok(img)
    // Save the image to a bmp file
    // let _ = img.save(bmp_path);
    // println!("Generated: {}", bmp_path);
    // Ok(())
}
