#![allow(dead_code)]

use anyhow::Result;

use futures::prelude::*;
use irc::client::prelude::*;

use time::macros::offset;
use time::OffsetDateTime;

use std::fs::File;
use std::io::BufReader;
use std::process::ExitCode;

mod bmp_helper;
mod cli_arguments_parser;
mod string_rastoriser;
use crate::bmp_helper::flip_image;
use crate::cli_arguments_parser::{parse_cli_arguments, CliVariables};
use crate::string_rastoriser::{get_text_formatting, rasterise_string};

const COLOURS: [[u8; 3]; 6] = [
    // black
    [0, 0, 0],
    // red
    [255, 0, 0],
    // green
    [0, 255, 0],
    // blue
    [0, 0, 255],
    // yellow
    [255, 255, 0],
    // orange
    [255, 123, 0],
];

const RENDERING_CONST: RenderingValues = RenderingValues {
    screen_width: 640,
    screen_height: 400,
    chars_per_line: 25, // not monospcae font so kinda just educated guesswork
                        // chars_per_line: 8, // not monospcae font so kinda just educated guesswork
};

struct RenderingValues {
    screen_width: u32,
    screen_height: u32,
    chars_per_line: usize,
}

#[tokio::main]
async fn main() -> ExitCode {
    env_logger::init();
    let envs = parse_cli_arguments();
    // confirm and get first argument
    // Determine if argument is an executable path.
    // if config_path.exists() {
    // let command = format!("{} $(pwd)/img.bmp", args[1]);
    if let Err(err) = run(envs).await {
        eprintln!("{err:?}");
        return ExitCode::FAILURE;
    }
    // } else {
    //     println!("The path provided does not exist!");
    //     return ExitCode::FAILURE;
    // }
    ExitCode::SUCCESS
}

async fn run(cli_variables: CliVariables) -> Result<()> {
    // initialise font
    let text_formatting = get_text_formatting();

    // let mut client = Client::from_config(config).await?;
    let mut client = Client::from_config(cli_variables.irc_config).await?;
    client.identify()?;

    let mut stream = client.stream()?;

    while let Some(message) = stream.next().await.transpose()? {
        if let Command::PRIVMSG(ref _channel, ref message_string) = message.command {
            let date_time = OffsetDateTime::now_utc().to_offset(offset!(+1));
            let user = match message.source_nickname() {
                Some(nickname) => nickname,
                _ => "unknown",
            };
            if (user.eq("utah") || user.eq("sean")) && message_string.contains("now ready") {
                let mut rendered_string =
                    format!("{:0>2}:{:0>2}", date_time.hour(), date_time.minute());
                let red_length = rendered_string.chars().count();
                // rendered_string = format!("{} {}", rendered_string, textwrap::wrap(messageString, 28),);
                rendered_string = format!("{} {}", rendered_string, message_string);
                rendered_string = rendered_string.replace("now ready ", "");
                let mut colours = vec![0; rendered_string.chars().count()];
                for colour in colours.iter_mut().take(red_length) {
                    *colour = 1;
                }
                // for i in 0..red_length {
                //     colours[i] = 1;
                // }
                let img =
                    rasterise_string("img.bmp", &rendered_string, &mut colours, &text_formatting)?;
                match img.save("img.bmp") {
                    Ok(_) => {
                        let f = File::open("img.bmp")?;
                        let metadata = f.metadata()?;
                        let buffered_reader = BufReader::new(f);
                        let r = ureq::put(&format!(
                            "{}{}.bmp",
                            cli_variables.drive_folder_url, cli_variables.drive_file_prefix
                        ))
                        .set("Content-Length", &metadata.len().to_string())
                        .set("Authorization", &cli_variables.drive_auth_header)
                        .send(buffered_reader);
                        println!("{:?}", r);
                    }
                    Err(err) => {
                        println!("Failed to save img.bmp\n error: {}", err);
                    }
                }

                match flip_image(&img).save("img_180.bmp") {
                    Ok(_) => {
                        let f = File::open("img_180.bmp")?;
                        let metadata = f.metadata()?;
                        let buffered_reader = BufReader::new(f);
                        let r = ureq::put(&format!(
                            "{}{}_180.bmp",
                            cli_variables.drive_folder_url, cli_variables.drive_file_prefix
                        ))
                        .set("Content-Length", &metadata.len().to_string())
                        .set("Authorization", &cli_variables.drive_auth_header)
                        .send(buffered_reader);
                        println!("{:?}", r);
                    }
                    Err(err) => {
                        println!("Failed to save img_180.bmp\n error: {}", err);
                    }
                }
            }
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::get_text_formatting;
    use crate::rasterise_string;

    #[test]
    fn make_bmp1() {
        let rendered_string = "20:00 Smokey imp noon now ready on 3rd. Thanks carboardturkey!";
        let mut colours = vec![0; rendered_string.chars().count()];
        for i in 0..5 {
            colours[i] = 1;
        }
        assert!(rasterise_string(
            "make_bmp1.bmp",
            rendered_string,
            &mut colours,
            &get_text_formatting()
        )
        .is_ok());
    }

    #[test]
    fn make_bmp2_emoji() {
        let rendered_string = "01:02 Smokey imp noon ☕ now ready on 3rd. Thanks carboardturkey!";
        let mut colours = vec![0; rendered_string.chars().count()];
        for i in 0..5 {
            colours[i] = 1;
        }
        assert!(rasterise_string(
            "make_bmp2.bmp",
            rendered_string,
            &mut colours,
            &get_text_formatting()
        )
        .is_ok());
    }

    #[test]
    fn make_bmp3_very_long() {
        let rendered_string = "12:34 Smokey imp noon ☕ now ready on 3rd. Thanks carboardturkey! even more tea might be available on 4th but nobody really checks";
        let mut colours = vec![0; rendered_string.chars().count()];
        for i in 0..5 {
            colours[i] = 1;
        }
        assert!(rasterise_string(
            "make_bmp3.bmp",
            rendered_string,
            &mut colours,
            &get_text_formatting()
        )
        .is_ok());
    }

    #[test]
    fn make_bmp4_very_very_long() {
        let rendered_string =  "05:18 What happens to sean's screen if I give you a really really really really really really really really really really really really really really really really really really really really really really really really really really long tea name now ready on 3rd. Thanks poppy!";
        let mut colours = vec![0; rendered_string.chars().count()];
        for i in 0..5 {
            colours[i] = 1;
        }
        assert!(rasterise_string(
            "make_bmp4.bmp",
            rendered_string,
            &mut colours,
            &get_text_formatting()
        )
        .is_ok());
    }

    #[test]
    fn make_bmp5_multi_messages() {
        let rendered_string = "20:00 Smokey imp noon now ready on 3rd. Thanks carboardturkey!";
        let mut colours = vec![0; rendered_string.chars().count()];
        for i in 0..5 {
            colours[i] = 1;
        }
        assert!(rasterise_string(
            "make_bmp5.bmp",
            rendered_string,
            &mut colours,
            &get_text_formatting()
        )
        .is_ok());

        assert!(rasterise_string(
            "make_bmp5.bmp",
            rendered_string,
            &mut colours,
            &get_text_formatting()
        )
        .is_ok());
    }
}
