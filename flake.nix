{
  description = "Wrapper for wavesher e-ink C";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    flake-utils.url  = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    naersk.url = "github:nmattia/naersk/master";
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, naersk }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        naersk-lib = naersk.lib."${system}".override {
          cargo = pkgs.rust-bin.stable.latest.default;
          rustc = pkgs.rust-bin.stable.latest.default;
        };

      name = "paper-wrapper-rust";

      packages."${name}" = naersk-lib.buildPackage {
        pname = name;
        root = ./.;
        nativeBuildInputs = with pkgs; [
          openssl
          pkg-config
          rust-bin.stable.latest.default
          imagemagick
        ];
        doCheck = true;
      };

      apps."${name}" = flake-utils.lib.mkApp {
        drv = packages."${name}";
      };
      in {
        defaultPackage = packages."${name}";

        defaultApp = apps."${name}";

        devShell = with pkgs; mkShell {
          buildInputs = [
            openssl
            pkg-config
            rust-bin.stable.latest.default
            imagemagick
          ];
        };
      }
    );
}
